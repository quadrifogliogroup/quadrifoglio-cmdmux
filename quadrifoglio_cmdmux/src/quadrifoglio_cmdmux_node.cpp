#include <string>
#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Joy.h>
#include <std_msgs/Int8.h>

ros::Duration failsafeTime(0.5); //If a topic is silent for this time, it is assumed dead
                                //and a zeroed drive message is published in its stead



class CmdMux{
    public:
    CmdMux(ros::Publisher* cmdVelPub, ros::Publisher* controlChannelPub){

        _cmdVelPub = cmdVelPub;
        _controlChannelPub = controlChannelPub;
        _channel = 0;

        //Put timestamps in the past so the topics count as inactive from the start
        _lastVelJoy = ros::Time::now()-ros::Duration(10);
        _lastVelBi = ros::Time::now()-ros::Duration(10);
        _lastJoy = ros::Time::now()-ros::Duration(10);

    }
    void ccPubTimerCb(const ros::TimerEvent&){
        std_msgs::Int8 msg;
        msg.data = _channel;
        _controlChannelPub->publish(msg);
    }

    void failsafeTimerCb(const ros::TimerEvent&){
        bool teleopOk = ros::Time::now()-_lastVelJoy < failsafeTime;
        bool autoOk = ros::Time::now()-_lastVelBi < failsafeTime;
        bool joyOk = ros::Time::now()-_lastVelJoy < failsafeTime;

        geometry_msgs::Twist zeroDrive;  //Zero-initialized drive command to send

        //If no joy message, force channel to 0 (zero output)
        if(!joyOk){
            _channel = 0;
            ROS_WARN_THROTTLE(5,"No messages on joy topic! Safety override, cmd_vel zeroed!");
        }

        //If no drive is chosen, publish a zero drive message
        if(_channel == 0){
            _cmdVelPub->publish(zeroDrive);
        }

        //If teleop is chosen, publish a zero drive message if the teleop topic is dead
        if(_channel == 1 && !teleopOk){
            _cmdVelPub->publish(zeroDrive);
            ROS_WARN_THROTTLE(5,"No messages on cmd_vel_joy topic! Publishing zero.");
        }

        //If auto mode is chosen, publish a zero drive message if the auto topic is dead
        if(_channel == 2 && !autoOk){
            _cmdVelPub->publish(zeroDrive);
            ROS_WARN_THROTTLE(5,"No messages on cmd_vel_blindiot topic! Publishing zero.");
        }
        

    }

    //Callback for teleop commands, index 1
    void cmdVelJoyCb(const geometry_msgs::Twist::ConstPtr& twistIn){
        _lastVelJoy = ros::Time::now();
        if(_channel == 1){
            _cmdVelPub->publish(*twistIn);
        }
    }

    //Callback for blindiot drive command, index 2
    void cmdVelBiCb(const geometry_msgs::Twist::ConstPtr& twistIn){
        _lastVelBi = ros::Time::now();
        if(_channel == 2){
            _cmdVelPub->publish(*twistIn);
        }
    }

    //Callback for joystick messages
    void joyCb(const sensor_msgs::Joy::ConstPtr& joyIn){
        _lastJoy = ros::Time::now();
        int wantedChannel = 0;  //If no joy buttons pressed, default to zero output
        if(joyIn->buttons.size()>=4){
            if(joyIn->buttons[3]){
                wantedChannel = 2;  //Auto mode
            }
            if(joyIn->buttons[2]){
                wantedChannel = 1;  //Manual mode, overrides auto
            }
        }
        if(wantedChannel != _channel){ //Changing channel, print out info
            if(wantedChannel == 0) ROS_INFO("Drive off! Sending zeroes to /cmd_vel");
            if(wantedChannel == 1) ROS_INFO("Teleop on! Sending /cmd_vel_joy to /cmd_vel");
            if(wantedChannel == 2) ROS_INFO("Auto mode! Sending /cmd_vel_blindiot to /cmd_vel");
        }
        _channel = wantedChannel;

    }

    private:
    ros::Publisher* _cmdVelPub;
    ros::Publisher* _controlChannelPub;

    ros::Time _lastVelJoy;  //Timestamps for last receive time of these messages
    ros::Time _lastVelBi;
    ros::Time _lastJoy;

    int _channel; //This indicates which channel to listen to (0=nothing 1=teleop 2=lauto)

};


int main(int argc, char **argv){

    ros::init(argc, argv, "quadrifoglio_cmdmux");
    ros::NodeHandle n;

    ros::Publisher cmdVelPub = n.advertise<geometry_msgs::Twist>("cmd_vel", 1);
    ros::Publisher controlChannelPub = n.advertise<std_msgs::Int8>("control_channel" ,1);

    CmdMux cmdMux( &cmdVelPub , &controlChannelPub);

    ros::Subscriber cmdVelJoySub = n.subscribe<geometry_msgs::Twist>("cmd_vel_joy", 1, &CmdMux::cmdVelJoyCb, &cmdMux);
    ros::Subscriber cmdVelBiSub = n.subscribe<geometry_msgs::Twist>("cmd_vel_blindiot", 1, &CmdMux::cmdVelBiCb, &cmdMux);
    ros::Subscriber joySub = n.subscribe<sensor_msgs::Joy>("joy", 1, &CmdMux::joyCb, &cmdMux);

    //If none of the input topics or joystick is updating, start sending a zeroed cmd_vel message
    ros::Timer failsafeTimer = n.createTimer(ros::Duration(0.033), &CmdMux::failsafeTimerCb, &cmdMux);
    ros::Timer controlChannelPubTimer = n.createTimer(ros::Duration(0.5), &CmdMux::ccPubTimerCb, &cmdMux);

    ros::spin();

    return 0;
}
