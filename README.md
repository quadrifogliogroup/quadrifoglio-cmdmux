# Quadrifoglio pointclouder

Subscribes to cmd_vel sources (teleop & auto) and passes them to the base according to a joy switch

Subscribes:
    cmd_vel_joy (teleop source)
    cmd_vel_blindiot (auto drive source)

Publishes:
    cmd_vel (base drive command)

Telep is passed when joy switch 2 is active
Auto is passed when joy switch 3 is active (and 2 is not)